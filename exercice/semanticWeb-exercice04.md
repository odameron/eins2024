---
title: "Exercice 4 : simple SPARQL queries on an RDF graph"
tags: eins2023, teaching, RDF, SPARQL
version: 1.0
date: 2023-07-04
---


# 4.0 Start the SPARQL server and load the data


## 4.0.1 data

We will use the file [`exercice04.ttl`](exercice04.ttl) which represents proteins and the biological processes from the [Gene Ontology](http://geneontology.org/) they are involved in.

![Direct GeneOntology annotations](../figures/goAnnotations-direct.png)






## 4.0.2 start the SPARQL server

(install GraphDB, e.g. in `/usr/local/semanticWeb`. The `graphdb-X.Y.Z` directory will be called `${GRAPHDB_HOME}`)
- from a terminal, run `${GRAPHDB_HOME}/bin/graphdb` (do not close the terminal)
- open http://localhost:7200 in a Web browser


## 4.0.3 load the dataset

- "setup" menu on the left
- repositories item
- (+) create new repository
	- choose "GraphDB repository"
	- repositoryID: "sparqlEINS"
	- ruleset: "RDFS" or "RDFS plus"
	- create :-)
	- make sure that it is running
- "import" menu on the left
	- upload RDF files
	- browse and select `exercice04.ttl`
	- import
- "SPARQL" menu on the left

## 4.0.4 SPARQL query template

In the following questions, you can use the following SPARQL query template:

```sparql
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
PREFIX owl: <http://www.w3.org/2002/07/owl#> 
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
PREFIX dct: <http://purl.org/dc/terms/> 

PREFIX go: <http://purl.obolibrary.org/obo/GO_>
PREFIX goavoc: <http://bio2rdf.org/goa_vocabulary:>

PREFIX ex: <http://example.org/tutorial/>

# *=all the variables in the WHERE clause.
# Use a space-separated list of variables
# if you are only interested in some of them
# or if you want to specify their order
SELECT * 
WHERE {
  # your graph pattern goes here
}
```


# 4.1 Write the SPARQL query that retrieves the name of p1

Expected result: "Protein 1" 


```bash
# SOLUTION:
echo "UFJFRklYIHJkZjogPGh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyM+IApQUkVGSVggcmRmczogPGh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDEvcmRmLXNjaGVtYSM+IApQUkVGSVggb3dsOiA8aHR0cDovL3d3dy53My5vcmcvMjAwMi8wNy9vd2wjPiAKUFJFRklYIHhzZDogPGh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIz4gClBSRUZJWCBkY3Q6IDxodHRwOi8vcHVybC5vcmcvZGMvdGVybXMvPiAKClBSRUZJWCBnbzogPGh0dHA6Ly9wdXJsLm9ib2xpYnJhcnkub3JnL29iby9HT18+ClBSRUZJWCBnb2F2b2M6IDxodHRwOi8vYmlvMnJkZi5vcmcvZ29hX3ZvY2FidWxhcnk6PgoKUFJFRklYIGV4OiA8aHR0cDovL2V4YW1wbGUub3JnL3R1dG9yaWFsLz4KClNFTEVDVCA/cHJvdGVpbkxhYmVsIApXSEVSRSB7CiAgZXg6cDEgcmRmczpsYWJlbCA/cHJvdGVpbkxhYmVsIC4KfQo=" | base64 --decode
```


# 4.2 Write the SPARQL query that retrieves all the proteins and their name

Expected result:

| ?protein | ?proteinLabel |
| --- | --- |
| ex:p1 | "Protein 1" |
| ex:p2 | "Protein 2" |
| ex:p3 | "Protein 3" |
| ex:p4 | "Protein 4" |
| ex:p5 | "Protein 5" |



```bash
# SOLUTION:
echo "UFJFRklYIHJkZjogPGh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyM+IApQUkVGSVggcmRmczogPGh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDEvcmRmLXNjaGVtYSM+IApQUkVGSVggb3dsOiA8aHR0cDovL3d3dy53My5vcmcvMjAwMi8wNy9vd2wjPiAKUFJFRklYIHhzZDogPGh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIz4gClBSRUZJWCBkY3Q6IDxodHRwOi8vcHVybC5vcmcvZGMvdGVybXMvPiAKClBSRUZJWCBnbzogPGh0dHA6Ly9wdXJsLm9ib2xpYnJhcnkub3JnL29iby9HT18+ClBSRUZJWCBnb2F2b2M6IDxodHRwOi8vYmlvMnJkZi5vcmcvZ29hX3ZvY2FidWxhcnk6PgoKUFJFRklYIGV4OiA8aHR0cDovL2V4YW1wbGUub3JnL3R1dG9yaWFsLz4KClNFTEVDVCA/cHJvdGVpbiA/cHJvdGVpbkxhYmVsIApXSEVSRSB7CiAgP3Byb3RlaW4gcmRmOnR5cGUgZXg6UHJvdGVpbiAuCiAgP3Byb3RlaW4gcmRmczpsYWJlbCA/cHJvdGVpbkxhYmVsIC4KfQo=" | base64 --decode
```


# 4.3 Write the SPARQL query that retrieves  the URI of the GeneOntology class which name is "gluconeogenesis"

Expected result: `go:0006094`

```bash
# SOLUTION:
echo "UFJFRklYIHJkZjogPGh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyM+IApQUkVGSVggcmRmczogPGh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDEvcmRmLXNjaGVtYSM+IApQUkVGSVggb3dsOiA8aHR0cDovL3d3dy53My5vcmcvMjAwMi8wNy9vd2wjPiAKUFJFRklYIHhzZDogPGh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIz4gClBSRUZJWCBkY3Q6IDxodHRwOi8vcHVybC5vcmcvZGMvdGVybXMvPiAKClBSRUZJWCBnbzogPGh0dHA6Ly9wdXJsLm9ib2xpYnJhcnkub3JnL29iby9HT18+ClBSRUZJWCBnb2F2b2M6IDxodHRwOi8vYmlvMnJkZi5vcmcvZ29hX3ZvY2FidWxhcnk6PgoKUFJFRklYIGV4OiA8aHR0cDovL2V4YW1wbGUub3JnL3R1dG9yaWFsLz4KClNFTEVDVCA/Z28gCldIRVJFIHsKICA/Z28gcmRmczpsYWJlbCAiZ2x1Y29uZW9nZW5lc2lzIiAuCn0K" | base64 --decode
```


# 4.4  For each protein, retrieve their name and the identifier and the name of their GeneOntology direct annotations

Expected result:

| ?protein | ?proteinLabel | ?go | ?goLabel |
|---|---|---|---|
|[ex:p1](http://localhost:7200/resource?uri=http%3A%2F%2Fexample.org%2Ftutorial%2Fp1 "http://example.org/tutorial/p1")|"Protein P1"|[go:0005996](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0005996 "http://purl.obolibrary.org/obo/GO_0005996")|"monosaccharide metab proc"|
|[ex:p2](http://localhost:7200/resource?uri=http%3A%2F%2Fexample.org%2Ftutorial%2Fp2 "http://example.org/tutorial/p2")|"Protein P2"|[go:0019318](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0019318 "http://purl.obolibrary.org/obo/GO_0019318")|"hexose metab proc"|
|[ex:p3](http://localhost:7200/resource?uri=http%3A%2F%2Fexample.org%2Ftutorial%2Fp3 "http://example.org/tutorial/p3")|"Protein P3"|[go:0006094](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0006094 "http://purl.obolibrary.org/obo/GO_0006094")|"gluconeogenesis"|
|[ex:p4](http://localhost:7200/resource?uri=http%3A%2F%2Fexample.org%2Ftutorial%2Fp4 "http://example.org/tutorial/p4")|"Protein P4"|[go:0006094](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0006094 "http://purl.obolibrary.org/obo/GO_0006094")|"gluconeogenesis"|
|[ex:p4](http://localhost:7200/resource?uri=http%3A%2F%2Fexample.org%2Ftutorial%2Fp4 "http://example.org/tutorial/p4")|"Protein P4"|[go:0002376](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0002376 "http://purl.obolibrary.org/obo/GO_0002376")|"immune system response"|
|[ex:p5](http://localhost:7200/resource?uri=http%3A%2F%2Fexample.org%2Ftutorial%2Fp5 "http://example.org/tutorial/p5")|"Protein P5"|[go:0002376](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0002376 "http://purl.obolibrary.org/obo/GO_0002376")|"immune system response"|
|[ex:p5](http://localhost:7200/resource?uri=http%3A%2F%2Fexample.org%2Ftutorial%2Fp5 "http://example.org/tutorial/p5")|"Protein P5"|[go:0035172](http://localhost:7200/resource?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FGO_0035172 "http://purl.obolibrary.org/obo/GO_0035172")|"hematocyte prolif"|

```bash
# SOLUTION:
echo "UFJFRklYIHJkZjogPGh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyM+IApQUkVGSVggcmRmczogPGh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDEvcmRmLXNjaGVtYSM+IApQUkVGSVggb3dsOiA8aHR0cDovL3d3dy53My5vcmcvMjAwMi8wNy9vd2wjPiAKUFJFRklYIHhzZDogPGh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIz4gClBSRUZJWCBkY3Q6IDxodHRwOi8vcHVybC5vcmcvZGMvdGVybXMvPiAKClBSRUZJWCBnbzogPGh0dHA6Ly9wdXJsLm9ib2xpYnJhcnkub3JnL29iby9HT18+ClBSRUZJWCBnb2F2b2M6IDxodHRwOi8vYmlvMnJkZi5vcmcvZ29hX3ZvY2FidWxhcnk6PgoKUFJFRklYIGV4OiA8aHR0cDovL2V4YW1wbGUub3JnL3R1dG9yaWFsLz4KClNFTEVDVCA/cHJvdGVpbiA/cHJvdGVpbkxhYmVsID9nbyA/Z29MYWJlbApXSEVSRSB7CiAgP3Byb3RlaW4gcmRmOnR5cGUgZXg6UHJvdGVpbiAuCiAgP3Byb3RlaW4gcmRmczpsYWJlbCA/cHJvdGVpbkxhYmVsIC4KICA/cHJvdGVpbiBnb2F2b2M6cHJvY2VzcyA/Z28gLgogID9nbyByZGZzOmxhYmVsID9nb0xhYmVsIC4KfQo=" | base64 --decode
```
