---
title: "Exercice 5: representation of ontologies in RDF"
tags: eins2023, teaching, RDF, ontologies, RDFS, OWL, Protégé
version: 1.1
date: 2023-07-07
---

# 5.1 Create an empty ontology with Protégé

- define the default namespace as `http://example.org/tutorial/` (do not forget the trailing `/`)
- add some annotations describing your ontology
	- `rdfs:label` for the name of your ontology
	- `rdfs:comment` for a textual description
	- `owl:versionInfo` for versioning
- save in turtle format

![Simple empty ontology in Protégé](../figures/protege-simpleOntology-empty.png)

```turtle
@prefix : <http://www.semanticweb.org/olivier/ontologies/2023/6/untitled-ontology-58/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@base <http://www.semanticweb.org/olivier/ontologies/2023/6/untitled-ontology-58/> .

<http://example.org/tutorial/> rdf:type owl:Ontology ;
                                rdfs:comment "This simple ontology will allow us to see how knowledge formalized by classes and their relations is represented in RDF"@en ;
                                rdfs:label "My tutorial ontology"@en ;
                                owl:versionInfo 1.0 .
```

> **Interpretation:** Creating an (OWL) ontology actually consists in creating a resource and make it in instance of `owl:Ontology`. Describing this ontology follows the classical RDF approach.


# 5.2 Create sibling classes at the root of the ontology

- create the three classes `Molecule`, `Protein` and `TranscriptionFactor` at the root of the ontology 
	- their identifiers do not matter
	- (bonus: provide their `rdfs:label`)
- save again in turtle format
- observe how classes are represented

![Simple ontology in Protégé where all the classes are siblings](../figures/protege-simpleOntology-classesSiblings.png)

```turtle
@prefix : <http://www.semanticweb.org/olivier/ontologies/2023/6/untitled-ontology-58/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@base <http://www.semanticweb.org/olivier/ontologies/2023/6/untitled-ontology-58/> .

<http://example.org/tutorial/> rdf:type owl:Ontology ;
                                rdfs:comment "This simple ontology will allow us to see how knowledge formalized by classes and their relations is represented in RDF"@en ;
                                rdfs:label "My tutorial ontology"@en ;
                                owl:versionInfo 1.0 .

#################################################################
#    Classes
#################################################################

###  http://example.org/tutorial/TranscriptionFactor
:TranscriptionFactor rdf:type owl:Class ;
         rdfs:label "Transcription factor"@en .


###  http://example.org/tutorial/Molecule
:Molecule rdf:type owl:Class ;
          rdfs:label "Molecule"@en .


###  http://example.org/tutorial/Protein
:Protein rdf:type owl:Class ;
         rdfs:label "Protein"@en .
```

> **Interpretation:** Creating classes in an ontology simply consists in declaring resources as instances of the (meta-)class `owl:Class`. Not that because in the specification `owl:Class` is a subclass of `rdfs:Class`, all the OWL classes are also RDFS classes.


# 5.3 Organize your classes in a hierarchy

- drag and drop (order not relevant)
	- `TranscriptionFactor` on `Protein` 
	- `Protein` on `Molecule`
- save the ontology

![Simple ontology in Protégé where the classes are organized in a hierarchy](../figures/protege-simpleOntology-classesHierarchy.png)

```turtle
@prefix : <http://www.semanticweb.org/olivier/ontologies/2023/6/untitled-ontology-58/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@base <http://www.semanticweb.org/olivier/ontologies/2023/6/untitled-ontology-58/> .

<http://example.org/tutorial/> rdf:type owl:Ontology ;
                                rdfs:comment "This simple ontology will allow us to see how knowledge formalized by classes and their relations is represented in RDF"@en ;
                                rdfs:label "My tutorial ontology"@en ;
                                owl:versionInfo 1.0 .

#################################################################
#    Classes
#################################################################

###  http://example.org/tutorial/TranscriptionFactor
:TranscriptionFactor rdf:type owl:Class ;
         rdfs:subClassOf :Protein ;
         rdfs:label "Transcription factor"@en .


###  http://example.org/tutorial/Molecule
:Molecule rdf:type owl:Class ;
          rdfs:label "Molecule"@en .


###  http://example.org/tutorial/Protein
:Protein rdf:type owl:Class ;
         rdfs:subClassOf :Molecule ;
         rdfs:label "Protein"@en .
```

> **Interpretation:** Composing the class hierarchy of an ontology consists in declaring that a class is a subclass of one or more superclasses with `rdfs:subClassOf`. Class inheritance is the main point of RDFS and is therefore also valid in OWL.
