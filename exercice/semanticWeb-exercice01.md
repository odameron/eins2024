---
title: "Exercice 1 : understand an RDF graph"
tags: eins2023, teaching, RDF
version: 1.0
date: 2023-07-04
---

Consider the following RDF graph:
![Simple RDF graph](../figures/rdf-graphInterpretation.png)

# 1.1 How many triples are there?

```bash
# SOLUTION:
echo "OSB0cmlwbGVzCg==" | base64 --decode
```

# 1.2 What are the resources?

```bash
# SOLUTION:
echo "CjggcmVzb3VyY2VzIChhIHJlc291cmNlIGlzIGFueXRoaW5nIHRoYXQgaGFzIGFuIFVSSSk6Ci0gZXg6UHJvdGVpbgotIGV4OnAxCi0gZXg6cDIKLSBleDpwMwotIHJkZjp0eXBlCi0gcmRmczpsYWJlbAotIGV4OmFjdGl2YXRlcwotIGV4OmluaGliaXRlcwo=" | base64 --decode
```



# 1.3 What are the classes? How do you recognize them?

```bash
# SOLUTION:
echo "MSBjbGFzczogZXg6UHJvdGVpbgpDbGFzc2VzIGFyZSB0aGUgb2JqZWN0cyAoM3JkIGVsZW1lbnQpIG9mIHRoZSB0cmlwbGVzIGhhdmluZyByZGY6dHlwZSBhcyB0aGUgcHJvcGVydHkgKDJuZCBlbGVtZW50KQo=" | base64 --decode
```



# 1.4 What are the instances? How do you recognize them?

```bash
# SOLUTION:
echo "CjMgaW5zdGFuY2VzOgotIGV4OnAxCi0gZXg6cDIKLSBleDpwMwpJbnN0YW5jZXMgYXJlIHRoZSBzdWJqZWN0cyAoMXN0IGVsZW1lbnQpIG9mIHRoZSB0cmlwbGVzIGhhdmluZyByZGY6dHlwZSBhcyB0aGUgcHJvcGVydHkgKDJuZCBlbGVtZW50KQo=" | base64 --decode
```


# 1.5 What are the properties?

```bash
# SOLUTION:
echo "NCBwcm9wZXJ0aWVzIChzb21lIG9mIHdoaWNoIG1heSBhcHBlYXIgaW4gc2V2ZXJhbCB0cmlwbGVzKToKLSByZGY6dHlwZQotIHJkZnM6bGFiZWwKLSBleDphY3RpdmF0ZXMKLSBleDppbmhpYml0cwo=" | base64 --decode
```


# 1.6 Narrate the information provided by the graph

```bash
# SOLUTION:
echo "ZXg6cDEsIGV4OnAyIGFuZCBleDpwMyBhcmUgdGhyZWUgcHJvdGVpbnMgY2FsbGVkIHJlc3BlY3RpdmVseSAiUHJvdGVpbiAxIiwgIlByb3RlaW4gMiIgYW5kICJQcm90ZWluIDMiLgpleDpwMSBhY3RpdmF0ZXMgZXg6cDIsIHdoaWNoIGFjdGl2YXRlcyBleDpwMywgd2hpY2ggaW5oaWJpdHMgZXg6cDEuCg==" | base64 --decode
```
