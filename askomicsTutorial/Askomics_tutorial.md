---
title: "Askomics tutorial"
tags: eins2024, teaching, RDF, SPARQL, askomics
version: 1.0
date: 2024-05-31
---

# 1. Principles for converting a table into RDF

## 1.1 Represent the information from the table into RDF triples

| Person | name        |
| :----- | ----------- |
| alice  | Alice Alpha |
| bob    | Bob Bravo   |
| denis  | Denis Delta |

```bash
# SOLUTION:
echo "ClBSRUZJWCByZGY6IDxodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjPiAKUFJFRklYIHJkZnM6IDxodHRwOi8vd3d3LnczLm9yZy8yMDAwLzAxL3JkZi1zY2hlbWEjPgpQUkVGSVggZXg6IDxodHRwOi8vZXhhbXBsZS5vcmcvdHV0b3JpYWwvPgoKZXg6YWxpY2UgcmRmOnR5cGUgZXg6UGVyc29uIC4KZXg6YWxpY2UgcmRmczpsYWJlbCBBbGljZSBBbHBoYSAuCg==" | base64 --decode
```


## 1.2 Which rules did you apply?

- Rule1:
- Rule2:

```bash
# SOLUTION:
echo "Ci0gUnVsZTE6IAogICAgLSB0aGUgZmlyc3QgY29sdW1uIHNwZWNpZmllcyB0aGUgc3ViamVjdCAoQWxpY2UsIEJvYiwuLi4pOyAKICAgIC0gdGhlIGhlYWRlciBvZiB0aGUgc2Vjb25kIGNvbHVtbiBzcGVjaWZpZXMgdGhlIHJlbGF0aW9uIChuYW1lKTsgCiAgICAtIGVhY2ggdmFsdWUgaW4gdGhlIHNlY29uZCBjb2x1bW4gc3BlY2lmaWVzIHRoZSB2YWx1ZSBvZiB0aGUgbmFtZSBmb3IgdGhlIGNvcnJlc3BvbmRpbmcgc3ViamVjdCBmcm9tIHRoZSBmaXJzdCBjb2x1bW4uCgotIFJ1bGUyOiB0aGUgaGVhZGVyIG9mIHRoZSBmaXJzdCBjb2x1bW4gc3BlY2lmaWVzIHRoZSB0eXBlIG9mIHRoZSByZXNvdXJjZXMgZGVzY3JpYmVkIGluIHRoZSB0YWJsZS4gSGVyZSwgaXQgdGVsbHMgdXMgdGhhdCBhbGljZSwgYm9iLC4uLiBhcmUgYWxsIGluc3RhbmNlcyBvZiB0aGUgUGVyc29uIGNsYXNzLgogICAgLSBmb3IgZWFjaCBsaW5lCiAgICAgICAgLSB0aGUgc3ViamVjdCBpcyB0aGUgdmFsdWUgaW4gdGhlIGZpcnN0IGNvbHVtbgogICAgICAgIC0gdGhlIHJlbGF0aW9uIGlzIHJkZjp0eXBlCiAgICAgICAgLSB0aGUgb2JqZWN0IGlzIHRoZSBoZWFkZXIgb2YgdGhlIGZpcnN0IGNvbHVtbgo=" | base64 --decode
```


## 1.3 Apply the rules from the previous question to the following table

| Person  | sex    |
| :------ | ------ |
| alice   | female |
| bob     | male   |
| charles | male   |



## 1.4 Some tables can be joined (and we can generalize one of the rules)

Since each person is associated to at most one line in each table (they are primary keys), we can merge the tables (this is called a "join" operation).

| Person  | name        | sex    | birthdate  |
| :------ | ----------- | ------ | ---------- |
| alice   | Alice Alpha | female | 1996-10-18 |
| bob     | Bob Bravo   | male   | 1978-01-21 |
| charles |             | male   | 1946-06-17 |
| denis   | Denis Delta |        | 1990-04-04 |


Adapt the rule(s) to handle tables with two or more columns

```bash
# SOLUTION:
echo "Ci0gUnVsZTE6IAogICAgLSBmb3IgZWFjaCBjb2x1bW4gZnJvbSB0aGUgc2Vjb25kCiAgICAgICAgLSB0aGUgZmlyc3QgY29sdW1uIHNwZWNpZmllcyB0aGUgc3ViamVjdCAoQWxpY2UsIEJvYiwuLi4pOyAKICAgICAgICAtIHRoZSBoZWFkZXIgb2YgdGhlIGNvbHVtbiBzcGVjaWZpZXMgdGhlIHJlbGF0aW9uIChuYW1lLCBzZXgsLi4uKTsgCiAgICAgICAgLSBlYWNoIHZhbHVlIGluIHRoZSBjb2x1bW4gc3BlY2lmaWVzIHRoZSB2YWx1ZSBvZiB0aGUgcmVsYXRpb24gZm9yIHRoZSBjb3JyZXNwb25kaW5nIHN1YmplY3QgZnJvbSB0aGUgZmlyc3QgY29sdW1uLgoKLSBSdWxlMjogdGhlIGhlYWRlciBvZiB0aGUgZmlyc3QgY29sdW1uIHNwZWNpZmllcyB0aGUgdHlwZSBvZiB0aGUgcmVzb3VyY2VzIGRlc2NyaWJlZCBpbiB0aGUgdGFibGUuIEhlcmUsIGl0IHRlbGxzIHVzIHRoYXQgYWxpY2UsIGJvYiwuLi4gYXJlIGFsbCBpbnN0YW5jZXMgb2YgdGhlIFBlcnNvbiBjbGFzcy4KICAgIC0gZm9yIGVhY2ggbGluZQogICAgICAgIC0gdGhlIHN1YmplY3QgaXMgdGhlIHZhbHVlIGluIHRoZSBmaXJzdCBjb2x1bW4KICAgICAgICAtIHRoZSByZWxhdGlvbiBpcyByZGY6dHlwZQogICAgICAgIC0gdGhlIG9iamVjdCBpcyB0aGUgaGVhZGVyIG9mIHRoZSBmaXJzdCBjb2x1bW4K" | base64 --decode
```


## 1.5 Load data into askomics

load the file `Person-sex-birthdate.tsv` into askomics:
- menu "Files"
- button "Cumputer"
- select `/home/bioinfo/EINS_2024/eins2024/askomicsTutorial/Person-sex-birthdate.tsv`
- buttons "Upload" and then "Close" (now the file has been uploaded into the askomics server)
- select the checkbox in front of the file
- button "Integrate"
- for gender, in the droplist select "Category" (and not just a string)
- for birthdate, make sure that the droplist has "Date"
- button "Integrate (public dataset)"
- when you have a green checkmark, you are all set! From the dataset menu, you can see that askomics generated 62 triples from your table

# 2. Simple queries about an entity

## 2.1 Retrieve all the instances of `Person`

- with AskOmics
	- menu Ask
	- select `Person` (well, at this point this is the only option :-)
	- button "Start"
	- in the graph, the "Person" vertex is selected
	- use the "Run and preview" for the first results, or "Run and save" to retrieve all the results (can be large depending on your dataset, hence the preview)
- display the SPARQL query that was generated
	- button "Run and save"
	- "Results" menu
	- retrieve your query (you can change its description)
	- button "SPARQL" ni the "Options" column (on the right side)

## 2.2 Retrieve all the instances of `Person`, their name, their sex and their birthdate


## 2.3 Retrieve all the instances of `Person`, and their name, their sex and their birthdate if we know them (Check Isabelle and Fred)


## 2.4 Retrieve the year Alice was born


## 2.5 Retrieve all the women


## 2.6 Retrieve all the women and they year they were born


## 2.7 Retrieve all the women born before 1980


## 2.8 Retrieve the sex and birthdate of all the persons whose name contains "is" (El**is**e, Den**is**, **Is**abelle)


## 2.x ~~Negation on datatypes: retrieve all the persons who are not male~~


# 3. Add relations between entities

## 3.1 Principles

- because a person can know several other persons or play multiple instruments, adding columns would require to duplicated some lines, so we need separate tables
- we also need to represent explicitly in the headers (after the '@') the type of the various relations' values

| Person | knows@Person |
| :----- | ------------ |
| alice  | bob          |
| alice  | charles      |
| alice  | isabelle     |
| bob    | denis        |
| bob    | elise        |
| denis  | alice        |
| ...    |              |

| Person  | plays@Instrument |
| :------ | ---------------- |
| alice   | harp             |
| alice   | piano            |
| alice   | ukulele          |
| bob     | guitar           |
| charles | guitar           |
| charles | piano            |
| charles | drum             |
| ...     |                  |

Integrate the files `Person-knows.tsv` and `Person-instrument.tsv`

![Graph of who knows whom and plays which intrument(s)](askomics-music.png)

## 3.2 who are the people that Alice knows?


## 3.3 who knows Alice?


## 3.4 what are the instruments that Alice plays?


## 3.5 who plays the guitar?


## 3.6 who plays the guitar and the piano ?

 
## 3.x ~~Negation on relations: who do not play the guitar?~~


## 3.x ~~Negation on relation values: who plays an instrument that is not the piano?~~


# 4. Add hierarchies

## 4.1 Principles

Ontologies can also be imported, either:
- as tabulated files with `rdfs:subClassOf`
- as regular RDF files (e.g. in turtle, or owl format)

Load `Instrument-subClass.tsv`

## 4.2 Who plays a string instrument?


